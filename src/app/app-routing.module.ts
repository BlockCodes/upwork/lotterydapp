import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from './Component/login/login.component'
import { HomeComponent } from './Component/home/home.component'
import { UserComponent } from './Component/user/user.component'
import { AdminComponent } from './Component/admin/admin.component'
import { AdminGuard } from './Guards/Admin/admin.guard'


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [ AdminGuard ]
  }
]

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule { }
