import { async } from '@angular/core/testing'
import { Component, OnInit } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3/web3.model'

@Component( {
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ]
} )
export class HomeComponent implements OnInit {


  account: string
  lot: any
  constructor (
    private web3service: Web3Service,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lot = data.lot
    } )
  }
  login = async () => {
    try {
      const admin = await this.lot.admin().call( {
        from: this.account
      } )
      console.log( 'Log: HomeComponent -> login -> admin', admin )
      if ( admin === this.account ) {
        this.route.navigateByUrl( '/admin' )
      } else {
        this.route.navigateByUrl( '/user' )
      }
    } catch ( error ) {
      console.log( 'Log: HomeComponent -> login -> error', error )
    }
  }
}
