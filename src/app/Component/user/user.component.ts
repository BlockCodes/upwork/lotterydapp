import { Component, OnInit, ViewChild } from "@angular/core";
import { FlatTreeControl } from "@angular/cdk/tree";
import {
  MatTreeFlattener,
  MatTreeFlatDataSource,
} from "@angular/material/tree";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { Web3Service } from "src/app/Services/Web3/web3.service";
import { Router } from "@angular/router";
import { Web3Model } from "src/app/Models/web3/web3.model";
import { DetailsModel, LotteryModel } from "src/app/Models/detail/detail.model";
import { DetailsClass, LotteryClass } from "src/app/Models/detail/detail.class";
import { ConvertService } from "src/app/Services/Convert/convert.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
})
export class UserComponent implements OnInit {
  displayedColumns: string[] = ["position", "name", "weight"];
  dataSource = null;
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  account: string;
  lot: any;

  Details: DetailsModel = new DetailsClass();

  LotteryTickets: LotteryModel[] = [];

  constructor(
    private conv: ConvertService,
    private web3service: Web3Service,
    private route: Router
  ) {}

  

  ngOnInit(): void {
    this.web3service.web3login();
    this.web3service.Web3Details$.subscribe(async (data: Web3Model) => {
      this.account = data.account;
      this.lot = data.lot;
      console.log("TCL: AdminComponent -> ngOnInit -> this.lot", this.lot);
    });
    this.view();
    this.dataSource.paginator = this.paginator;
  }

  view = async () => {
    const getDetails = await this.lot.contractDetails().call({
      from: this.account,
    });
    console.log("TCL: AdminComponent -> view -> getDetails", getDetails);
    this.Details.contract = getDetails.contractAddr;
    this.Details.participants = getDetails.partCount;
    this.Details.amount = await this.conv.toEther(getDetails.amount);
    this.Details.balance = await this.conv.getBalance(this.account);
    this.Details.winner = getDetails.winner;

    const getTickets = await this.lot.OwnedTickets().call({
      from: this.account,
    });
    this.LotteryTickets = []
    console.log("TCL: UserComponent -> view -> getTickets", getTickets);
    let pos = 0
    for (let i = 0; i < getTickets.length; i++) {
      if(getTickets[i] != "0"){
        pos++
        const ticket = new LotteryClass();
        ticket.position = pos;
        ticket.ticket = getTickets[i];
        this.LotteryTickets.push(ticket);
      }
    }
    this.dataSource = new MatTableDataSource<LotteryModel>(this.LotteryTickets);
    this.dataSource.paginator = this.paginator;
    console.log(
      "TCL: UserComponent -> view -> this.LotteryTickets",
      this.LotteryTickets
    );
  };

  buyLottery = async () => {
    try {
      const buy = await this.lot.BuyLottery().send({
        from: this.account,
        gas: 5000000,
        value: this.conv.toWei(this.Details.amount),
      });
      console.log("TCL: UserComponent -> buyLottery -> buy", buy);
      if (buy.status === true) {
        await this.view();
        alert(
          "You have successfully purachased the Lottery" +
            buy.events.NewLottery.returnValues.count
        );
        this.route.navigateByUrl("/user");
      }
    } catch (error) {
      console.log("TCL: UserComponent -> buyLottery -> error", error);
    }
  };

  claimAll = async () => {
    try {
      const claim = await this.lot.ClaimAll().send({
        from: this.account,
        gas: 5000000,
      });
      if (claim.status === true) {
        await this.view();
        alert("You have successfully claimed all your the Lottery Amount");
        this.route.navigateByUrl("/user");
      }
    } catch (error) {}
  };
  claim = async (ticket) => {
    try {
      const claim = await this.lot.Claim(ticket).send({
        from: this.account,
        gas: 5000000,
      });
      if (claim.status === true) {
        await this.view();
        alert(
          "You have successfully claimed your the Lottery Amount for Ticket No. " +
            ticket
        );
        this.route.navigateByUrl("/user");
      }
    } catch (error) {}
  };
}
