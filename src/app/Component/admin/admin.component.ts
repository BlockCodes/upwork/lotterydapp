import { Component, OnInit } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3/web3.model'
import { DetailsModel } from "src/app/Models/detail/detail.model"
import { NgForm } from '@angular/forms'
import { ConvertService } from 'src/app/Services/Convert/convert.service'

@Component( {
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [ './admin.component.scss' ]
} )
export class AdminComponent implements OnInit {


  account: string
  lot: any

  Details: DetailsModel = {
    contract: null,
    participants: 0,
    amount: 0,
    balance: 0,
    winner: 0
  };

  constructor (
    private conv: ConvertService,
    private web3service: Web3Service,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lot = data.lot
      console.log( "TCL: AdminComponent -> ngOnInit -> this.lot", this.lot )
    } )
    this.view()
  }

  view = async () => {
    const getDetails = await this.lot.contractDetails().call( {
      from: this.account,
    } )
    console.log( 'TCL: AdminComponent -> view -> getDetails', getDetails )
    this.Details.contract = getDetails.contractAddr
    this.Details.participants = getDetails.partCount
    this.Details.amount = await this.conv.toEther( getDetails.amount )
    this.Details.balance = await this.conv.getBalance( this.account )
    this.Details.winner = getDetails.winner
  }

  chooseWinner = async () => {
    try {
      const choose = await this.lot.ChooseWinner().send( {
        from: this.account, gas: 5000000
      } )
      if ( choose.status === true ) {
        await this.view()
        alert( 'Your Winner is Chosen : ' + choose.wonLottery )
        this.route.navigateByUrl( '/admin' )
      }
    } catch ( error ) {
    }
  }

  newEvent = async () => {
    try {
      const choose = await this.lot.NewEvent().send( {
        from: this.account, gas: 5000000
      } )
      if ( choose.status === true ) {
        await this.view()
        alert( 'Your new event is started' + choose.wonLottery )
        this.route.navigateByUrl( '/admin' )
      }
    } catch ( error ) {
    }
  }

  changeAdmin = async (form: NgForm) => {
    try {
      console.log("TCL: AdminComponent -> changeAdmin -> form", form.value)
      const change = await this.lot.ChangeAdmin( form.value.address ).send( {
        from: this.account, gas: 5000000
      } )
      if ( change.status === true ) {
        await this.view()
        alert( 'Your Winner is Chosen : ' + change.wonLottery )
        this.route.navigateByUrl( '/admin' )
      }
    } catch (error) {
    console.log("TCL: AdminComponent -> changeAdmin -> error", error)
  }
}

}
