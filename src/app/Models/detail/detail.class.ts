import { DetailsModel, LotteryModel } from 'src/app/Models/detail/detail.model'
export class DetailsClass implements DetailsModel {
  contract: string
  participants: number
  amount: number
  balance: any
  winner: number
}
export class LotteryClass implements LotteryModel{
  position: number
  ticket: number
}


