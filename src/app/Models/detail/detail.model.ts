export interface DetailsModel {
  contract: string
  participants: number
  amount: number
  balance: any
  winner: number
}

export interface LotteryModel {
  position: number
  ticket: number
}
