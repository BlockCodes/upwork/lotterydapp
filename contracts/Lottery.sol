pragma solidity ^0.6.4;
contract Lottery {
	constructor() public {
		admin = msg.sender;
	}
	address payable public admin;
	uint256 public lottery_amount = 10 ether;
	uint256 public lottery_tickets = 5;
	uint256 public count = 0;
	uint256 public claimCount = 0;
	uint256 public lottery_pool;
	uint256 public lottery_event = 1;
	uint256 public lastWinner;
	mapping(uint256 => mapping(uint256 => address payable)) ticket_holder;
	mapping(uint256 => mapping(address => uint256[])) owned_tickets;
	mapping(uint256 => mapping(address => uint256)) owned_ticket_count;

	modifier onlyAdmin() {
		require(msg.sender == admin, "Only admin can do it");
		_;
	}

	modifier buyLottery() {
		require(msg.value == lottery_amount, "Lottery amount must be 3 ether");
		require(count <= lottery_tickets, "Tickets sold out");
		_;
	}
	event NewLottery(uint count);
	event Winner(uint winner);

	function BuyLottery() external payable buyLottery {
		count++;
		ticket_holder[lottery_event][count] = msg.sender;
		owned_tickets[lottery_event][msg.sender].push(count);
		owned_ticket_count[lottery_event][msg.sender]++;
		lottery_pool = lottery_pool + msg.value;
		emit NewLottery(count);
	}

	function Claim(uint256 _lotteryNumber) external {
		ticket_holder[lottery_event][_lotteryNumber].transfer(lottery_amount);
		lottery_pool = lottery_pool - lottery_amount;
		delete ticket_holder[lottery_event][_lotteryNumber];
		delete owned_tickets[lottery_event][msg.sender][indexOf(
			owned_tickets[lottery_event][msg.sender],
			_lotteryNumber
		)];
				owned_ticket_count[lottery_event][msg.sender]--;
				claimCount++;
	}

	function ClaimAll() external {
	  msg.sender.transfer(owned_ticket_count[lottery_event][msg.sender] * lottery_amount);
	  lottery_pool =lottery_pool -(owned_ticket_count[lottery_event][msg.sender] * lottery_amount);
			claimCount = claimCount + owned_ticket_count[lottery_event][msg.sender];
			owned_ticket_count[lottery_event][msg.sender]= 0;
	  delete owned_tickets[lottery_event][msg.sender];
	}

	function ChooseWinner() public onlyAdmin{
		uint256 winner;
		do {
			winner =(uint256(keccak256(abi.encodePacked(now))) % (count)) +1;
			if (ticket_holder[lottery_event][winner] != address(0)) {
				ticket_holder[lottery_event][winner].transfer(
					(lottery_pool * 8) / 10
				);
				admin.transfer((lottery_pool * 2) / 10);
				lastWinner = winner;
				lottery_pool = 0;
				count = 0;
				claimCount = 0;
				lottery_event++;
				emit Winner(winner);
			}
		} while (lottery_pool != 0);
	}
	function NewEvent() public onlyAdmin {
		require((count-claimCount)==0,'partCount is not zero');
		lottery_pool = 0;
		count = 0;
		claimCount = 0;
		lottery_event++;
	}
	function ChangeAdmin(address payable _newAdmin) external onlyAdmin {
		admin = _newAdmin;
	}

	function indexOf(uint256[] memory self, uint256 value)
		internal
		pure
		returns (uint256)
	{
		for (uint256 i = 0; i < self.length; i++)
			if (self[i] == value) return i;
		return uint256(-1);
	}

	function OwnedTickets() external view returns (uint256[] memory) {
		return owned_tickets[lottery_event][msg.sender];
	}

	function TicketHolder(uint256 _ticket) external view returns (address) {
		return ticket_holder[lottery_event][_ticket];
	}

	function contractDetails() external view returns (address contractAddr, uint256 partCount, uint amount, uint winner){
	  return(address(this), (count-claimCount), lottery_amount, lastWinner);
	}
}
